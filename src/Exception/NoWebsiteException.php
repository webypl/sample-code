<?php
declare(strict_types = 1);

namespace App\Exception;

/**
 * @author Łukasz Łuasiewicz <lukasz@weby.pl>
 */
class NoWebsiteException extends \Exception
{
}