<?php
declare(strict_types = 1);

namespace App\Model\DTO;

/**
 * @author Łukasz Łuasiewicz <lukasz@weby.pl>
 */
class OrderDTO
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var int
     */
    private $totalPrice = 0;

    /**
     * @var ItemDTO[]
     */
    private $items = [];

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $website;

    public function __construct(string $website)
    {
        $this->website = $website;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getTotalPrice(): int
    {
        return $this->totalPrice;
    }

    /**
     * @param int $totalPrice
     */
    public function setTotalPrice(int $totalPrice): void
    {
        $this->totalPrice = $totalPrice;
    }

    /**
     * @return ItemDTO[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    public function addItem(ItemDTO $item): void
    {
        $this->items[] = $item;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return string|null
     */
    public function getWebsite(): ?string
    {
        return $this->website;
    }

}
