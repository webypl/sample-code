<?php
declare(strict_types = 1);

namespace App\Model\DTO;

/**
 * @author Łukasz Łuasiewicz <lukasz@weby.pl>
 */
class ItemDTO
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $price;

    public function __construct(int $id, string $name, int $price)
    {
        $this->id  = $id;
        $this->name = $name;
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->getName();
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

}