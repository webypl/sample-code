<?php
declare(strict_types = 1);

namespace App\Model;

/**
 * @author Łukasz Łuasiewicz <lukasz@weby.pl>
 */
interface OrderInterface
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @param ItemInterface $item
     */
    public function addItem(ItemInterface $item): void;

    /**
     * @return array
     */
    public function getItems(): array;

    /**
     * @return int
     */
    public function getTotalPrice(): int;

    /**
     * @param int $totalPrice
     */
    public function setTotalPrice(int $totalPrice): void;

    /**
     * @return string
     */
    public function getAddress(): string;

    /**
     * @return string
     */
    public function getSourceWebsite(): string;

}
