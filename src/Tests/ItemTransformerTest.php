<?php
declare(strict_types = 1);

namespace App\Tests;

use App\Entity\Item;
use App\Factory\ItemDTOFactory;
use App\Model\DTO\ItemDTO;
use App\Transformer\ItemTransformer;
use PHPUnit\Framework\TestCase;

/**
 * @author Łukasz Łuasiewicz <lukasz@weby.pl>
 */
class ItemTransformerTest extends TestCase
{
    public function testTransformItemDTOToEntity(): void
    {
        $transformer = new ItemTransformer(new ItemDTOFactory());
        $resultToTest = $transformer->transformItemDTOToEntity($this->getExampleItem());

        $this->assertInstanceOf(Item::class, $resultToTest);
    }

    private function getExampleItem(): ItemDTO
    {
        $itemDTOFactory = new ItemDTOFactory();
        $itemDTO = $itemDTOFactory->create(1,'just item', 100);

        return $itemDTO;
    }

}