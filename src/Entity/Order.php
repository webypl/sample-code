<?php
declare(strict_types = 1);

namespace App\Entity;


use App\Model\ItemInterface;
use App\Model\OrderInterface;

/**
 * @author Łukasz Łuasiewicz <lukasz@weby.pl>
 */
class Order implements OrderInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $totalPrice;

    /**
     * @var ItemInterface[]
     */
    private $items = [];

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $number;

    /**
     * @var string
     */
    private $sourceWebsite;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getTotalPrice(): int
    {
        return $this->totalPrice;
    }

    /**
     * @param int $totalPrice
     */
    public function setTotalPrice(int $totalPrice): void
    {
        $this->totalPrice = $totalPrice;
    }

    /**
     * @return ItemInterface[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    public function addItem(ItemInterface $item): void
    {
       $this->items[] = $item;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getNumber(): string
    {
        return $this->number;
    }

    /**
     * @param string $number
     */
    public function setNumber(string $number): void
    {
        $this->number = $number;
    }

    /**
     * @return string
     */
    public function getSourceWebsite(): string
    {
        return $this->sourceWebsite;
    }

    /**
     * @param string $sourceWebsite
     */
    public function setSourceWebsite(string $sourceWebsite): void
    {
        $this->sourceWebsite = $sourceWebsite;
    }

}
