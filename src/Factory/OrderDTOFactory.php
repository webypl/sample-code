<?php
declare(strict_types = 1);

namespace App\Factory;

use App\Model\DTO\OrderDTO;

/**
 * @author Łukasz Łuasiewicz <lukasz@weby.pl>
 */
class OrderDTOFactory
{
     /**
      * @var string
      */
     private $defaultWebsite;

    /**
     * OrderDTOFactory constructor.
     * @param $defaultWebsite
     */
     public function __construct($defaultWebsite)
     {
         $this->defaultWebsite = $defaultWebsite;
     }

    /**
     * @return OrderDTO
     */
    public function create(): OrderDTO
    {
        return new OrderDTO($this->defaultWebsite);
    }

}