<?php
declare(strict_types = 1);

namespace App\Factory;


use App\Model\DTO\ItemDTO;

/**
 * @author Łukasz Łuasiewicz <lukasz@weby.pl>
 */
class ItemDTOFactory
{
    /**
     * @param string $name
     * @param int $id
     * @param int $price
     * @return ItemDTO
     */
    public function create(int $id, string $name, int $price): ItemDTO
    {
        return new ItemDTO($id, $name, $price);
    }

}
