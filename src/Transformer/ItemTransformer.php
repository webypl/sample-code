<?php
declare(strict_types = 1);

namespace App\Transformer;


use App\Entity\Item;
use App\Factory\ItemDTOFactory;
use App\Model\DTO\ItemDTO;
use App\Model\ItemInterface;

/**
 * @author Łukasz Łuasiewicz <lukasz@weby.pl>
 */
class ItemTransformer
{
    /**
     * @var ItemDTOFactory
     */
    private $itemDTOFactory;

    public function __construct(ItemDTOFactory $itemDTOFactory)
    {
        $this->itemDTOFactory = $itemDTOFactory;
    }

    /**
     * @param ItemDTO $itemDTO
     * @return ItemInterface
     */
    public function transformItemDTOToEntity(ItemDTO $itemDTO): ItemInterface
    {
        $item = new Item();
        $item->setId($itemDTO->getId());
        $item->setName($itemDTO->getName());
        $item->setPrice($itemDTO->getPrice());

        return $item;
    }

    /**
     * @param ItemInterface $item
     * @return ItemDTO
     */
    public function transformEntityToItemDTO(ItemInterface $item): ItemDTO
    {
        $itemDTO = $this->itemDTOFactory->create($item->getId(), $item->getName(), $item->getPrice());

        return $itemDTO;
    }
}
