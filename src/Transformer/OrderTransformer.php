<?php
declare(strict_types = 1);

namespace App\Transformer;


use App\Entity\Order;
use App\Exception\NoWebsiteException;
use App\Factory\OrderDTOFactory;
use App\Model\DTO\OrderDTO;
use App\Model\OrderInterface;

/**
 * @author Łukasz Łuasiewicz <lukasz@weby.pl>
 */
class OrderTransformer
{
    /**
     * @var ItemTransformer
     */
    private $itemTransformer;

    /**
     * @var OrderDTOFactory
     */
    private $orderDTOFactory;

    /**
     * OrderTransformer constructor.
     * @param ItemTransformer $itemTransformer
     * @param OrderDTOFactory $orderDTOFactory
     */
    public function __construct(ItemTransformer $itemTransformer, OrderDTOFactory $orderDTOFactory)
    {
        $this->itemTransformer = $itemTransformer;
        $this->orderDTOFactory = $orderDTOFactory;
    }

    /**
     * @param OrderDTO $orderDTO
     * @return OrderInterface
     * @throws NoWebsiteException
     */
    public function transformOrderDTOToEntity(OrderDTO $orderDTO): OrderInterface
    {
        $order = new Order();

        if (null === $orderDTO->getWebsite()) {
            throw new NoWebsiteException("Order has no website set.");
        }
        $order->setSourceWebsite($orderDTO->getWebsite());
        $order->setTotalPrice($orderDTO->getTotalPrice());
        $order->setAddress($orderDTO->getAddress());
        $order->setNumber(uniqid());

        foreach ($orderDTO->getItems() as $itemDTO) {
            $order->addItem($this->itemTransformer->transformItemDTOToEntity($itemDTO));
        }

        return $order;
    }

    /**
     * @param OrderInterface $order
     * @return OrderDTO
     */
    public function transformEntityToOrderDTO(OrderInterface $order): OrderDTO
    {
        $orderDTO = $this->orderDTOFactory->create();
        $orderDTO->setId($order->getId());
        $orderDTO->setAddress($order->getAddress());
        $orderDTO->setTotalPrice($order->getTotalPrice());

        foreach ($order->getItems() as $item) {
            $orderDTO->addItem($this->itemTransformer->transformEntityToItemDTO($item));
        }

        return $orderDTO;
    }
}
