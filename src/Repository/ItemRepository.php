<?php
declare(strict_types = 1);

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * @author Łukasz Łuasiewicz <lukasz@weby.pl>
 */
class ItemRepository extends EntityRepository
{
}
